using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class UserStatus 
{
    public int level = 1;
    public int cointcount;
    public int starCount;
    // Start is called before the first frame update
   
    [Serializable]
    public class DecorData
    {
        public int tilePackIndex = 0;

        public string currentThemeID = "theme_1";
        public List<string> unlockedThemeIDs;
    }

    [Serializable]
    public class BoosterData
    {
        public int findMatchAdsCount;
        public int shuffleAdsCount;
        public int swapTexAdsCount;

        public int findMatchCount = 5;
        public int shuffleCount = 5;
        public int swapTexCount = 5;
    }
    [Serializable] 
    public class RewardData
    {
        public bool removedAds;
    }
    public class TempData
    {
        public bool rate;
    }




}
