using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class UserData 
{
    public static UserData current;

    public UserStatus userStatus = new UserStatus();

    public DecorData decorData = new DecorData();

}
